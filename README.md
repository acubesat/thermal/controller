# Active Thermal Control

Components and control logic for the active thermal control system of AcubeSAT's Payload.

## Attributions

Project avatar image attribution: <a href="https://www.flaticon.com/free-icons/controller" title="controller icons">Controller icons created by Freepik - Flaticon</a>

---

<div align="center">
<p>
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>
