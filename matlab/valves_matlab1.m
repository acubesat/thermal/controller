
model = createpde("thermal","transient");

importGeometry(model, "perpendicular flow valve mount.STL");
scale(model.Geometry,1E-3);

figure
pdegplot(model,"FaceLabels","on");
generateMesh(model, "Hmax", 0.01);



thermalProperties(model, "ThermalConductivity", 101.97, ...
                  "MassDensity", 2197.77, ...
                  "SpecificHeat", 1143.38);


thermalIC(model, -20);


% controller stuffs
goalTemp = 5;
Kp = 1;
Ki = 0.0;
Kd = 0.0;

previous_error = 0;
integral = 0;



pointOfInterest = [0.001; 0.0124; 0.037]; %temp_sensor reading

tlist = 0:1:18;

dt = 1;


for t = tlist
    % Interpolate temperature at the point of interest at time t
    T_current = interpolateTemperature(R1, pointOfInterest, t);

    % Calculate error
    error = setpointTemperature - T_current;

    % PID calculations
    integral = integral + error * dt;
    derivative = (error - previous_error) / dt;
    output = Kp * error + Ki * integral + Kd * derivative;
    previous_error = error;

    thermalBC(model, "Face", 9, "HeatFlux", output);
    
R1 = solve(model, tlist);

end



figure("units","normalized","outerposition",[0 0 1 1])
pdeplot3D(model,"ColorMapData",R1.Temperature(:,end))



TpointOfInterest = interpolateTemperature(R1,pointOfInterest,1:numel(tlist));





figure
plot(tlist,TpointOfInterest)
title("Temperature at the Bottom as a Function of Time")
xlabel("Time, s")
ylabel("Temperature, C")
grid on
